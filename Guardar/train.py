#EL PRESENTE ARCHIVO GENERA EL ENTRENAMIETO PERTINENTE DE CIFAR10
#CON VARIOS PARAMETROS A TOMAR EN CONCIDERACIÓN A LA HORA DEL ENTRENAMIENTO
# Integrantes: Juan Cuaycal
#			   Gissela Sangoquiza
#			   Michael Sarmiento
# 1.- Controla el guardado de las épocas de entrenamieto en formato hdf5
# 2.- Al tener un fállo de cualquier tipo  a la hora del entrenamiento, el modelo retomará el entrenamiento
#     desde la última epoca guardada.
# 3.-El modelo se cargará automaticamente sin necesidad de ingresar algún comando.

# IMPORTAMOS LOS PAQUETES NECESARIOS
import datetime

import matplotlib
import glob

from keras.utils import plot_model

matplotlib.use("Agg")
from pyimagesearch.callbacks.epochcheckpoint import EpochCheckpoint
from pyimagesearch.callbacks.trainingmonitor import TrainingMonitor
from keras.models import model_from_json
from keras.models import load_model
import keras.backend as K
import os
from keras.models import load_model
from keras.optimizers import SGD
import keras
from keras.datasets import cifar10
from keras.preprocessing.image import ImageDataGenerator
from keras.models import Sequential
from keras.layers import Dense, Dropout, Activation, Flatten
from keras.layers import Conv2D, MaxPooling2D

#----------------------------------------------------------------------------
#Parametros de Entrenamiento

latest_file='nada'
ultima_epoca=0
batch_size = 32
num_classes = 10
epocas = 11				#Número de Épocas
data_augmentation = True
num_predictions = 20		#Número de predicciones
save_dir = os.path.join(os.getcwd(), 'saved_models')
model_name = 'keras_cifar10_trained_model.h5'

#-------------------------------------------------------------------------------------------
# Datos divididos entre el entrenamiento y el conjuntos de prueba:
#Entrenamiento CIFAR10
(x_train, y_train), (x_test, y_test) = cifar10.load_data()

#Imprime el entrenamieto del modélo
print('x_train shape:', x_train.shape)
print(x_train.shape[0], 'train samples')
print(x_test.shape[0], 'test samples')

#-------------------------------------------------------------------------------------------
# Convierte vectores de clase en matrices de clase binaria.
y_train = keras.utils.to_categorical(y_train, num_classes)
y_test = keras.utils.to_categorical(y_test, num_classes)
#-------------------------------------------------------------------------------------------


# Si no se proporciona un punto de control de modelo específico, inicialice el entrenamiento
try:
    list_of_files = glob.glob('./output/checkpoints/*')
    latest_file = max(list_of_files, key=os.path.getctime)
    ultima_epoca = Numero = len(glob.glob('./output/checkpoints/*.hdf5'))
    #print (latest_file)
#Tome el último entrenamiento y comience desde ese punto
except:
    print(latest_file)
epocas_totales=epocas-ultima_epoca
print(epocas_totales)




if latest_file is'nada':
    #Compilamos el modelo desde el inicio

    print("[INFO] compilando modelo...")
    #  Definimos el modélo
    logdir = "logs/scalars/" + datetime.now().strftime("%Y%m%d-%H%M%S")
    tensorboard_callback = keras.callbacks.TensorBoard(log_dir=logdir)
    model = Sequential()
    model.add(Conv2D(32, (3, 3), padding='same',
                     input_shape=x_train.shape[1:]))
    model.add(Activation('relu'))
    model.add(Conv2D(32, (3, 3)))
    model.add(Activation('relu'))
    model.add(MaxPooling2D(pool_size=(2, 2)))
    model.add(Dropout(0.25))

    model.add(Conv2D(64, (3, 3), padding='same'))
    model.add(Activation('relu'))
    model.add(Conv2D(64, (3, 3)))
    model.add(Activation('relu'))
    model.add(MaxPooling2D(pool_size=(2, 2)))
    model.add(Dropout(0.25))

    model.add(Flatten())
    model.add(Dense(512))
    model.add(Activation('relu'))
    model.add(Dropout(0.5))
    model.add(Dense(num_classes))
    model.add(Activation('softmax'))

    # Inicialización de  RMSprop optimizer (dejaremos po defecto)
    opt = keras.optimizers.RMSprop()
    # Definimos el compilador
    # Comienza el entrenamiento con los parametros RMSprop
    model.compile(loss='categorical_crossentropy',
                  optimizer=opt,
                  metrics=['accuracy'])

    x_train = x_train.astype('float32')
    x_test = x_test.astype('float32')
    x_train /= 255
    x_test /= 255


#de lo contrario, estamos utilizando un modelo de punto de control
else:

    # cargar el punto de control desde el disco

    model = load_model(latest_file)

    print("[INFO] old learning rate: {}".format(
        K.get_value(model.optimizer.lr)))

    #se cambia el optimizador tomar en ceunta que aqui piede ir otrio según la necesidad o el mismo,ç
    # en este caso se cambio para mejorar ;la taza de aprenidaze (1e-2) antes se usaba en 1e-2
    K.set_value(model.optimizer.lr, 1e-2)
    print("[INFO] new learning rate: {}".format(
        K.get_value(model.optimizer.lr)))

# construir el camino hacia la trama de entrenamiento y el historial de entrenamiento
plotPath = os.path.sep.join(["output", "resnet_fashion_mnist.png"])
jsonPath = os.path.sep.join(["output", "resnet_fashion_mnist.json"])

# construir el conjunto de devoluciones de llamada
#callbaks aqui guardamos cada cuanto se gaurda rtamvin se debe cambiar en el ficherp pyumageserch/callbaks/EpochCheckpoint.py
callbacks = [
    EpochCheckpoint('./output/checkpoints', every=1,
        startAt=ultima_epoca)]

# entrenar a la red
print("[INFO] training network...")
# Entrenamiento de la RED
if not data_augmentation:
    print('Not using data augmentation.')
    model.fit(x_train, y_train,
              batch_size=batch_size,
              epochs=epocas_totales,
              validation_data=(x_test, y_test),
              callbacks=[tensorboard_callback],
              shuffle=True,
              )
else:
    print('Using real-time data augmentation.')
    # This will do preprocessing and realtime data augmentation:
    datagen = ImageDataGenerator(
        featurewise_center=False,  # set input mean to 0 over the dataset
        samplewise_center=False,  # set each sample mean to 0
        featurewise_std_normalization=False,  # divide inputs by std of the dataset
        samplewise_std_normalization=False,  # divide each input by its std
        zca_whitening=False,  # apply ZCA whitening
        zca_epsilon=1e-06,  # epsilon for ZCA whitening
        rotation_range=0,  # randomly rotate images in the range (degrees, 0 to 180)
        # randomly shift images horizontally (fraction of total width)
        width_shift_range=0.1,
        # randomly shift images vertically (fraction of total height)
        height_shift_range=0.1,
        shear_range=0.,  # set range for random shear
        zoom_range=0.,  # set range for random zoom
        channel_shift_range=0.,  # set range for random channel shifts
        # set mode for filling points outside the input boundaries
        fill_mode='nearest',
        cval=0.,  # value used for fill_mode = "constant"
        horizontal_flip=True,  # randomly flip images
        vertical_flip=False,  # randomly flip images
        # set rescaling factor (applied before any other transformation)
        rescale=None,
        # set function that will be applied on each input
        preprocessing_function=None,
        # image data format, either "channels_first" or "channels_last"
        data_format=None,
        # fraction of images reserved for validation (strictly between 0 and 1)
        validation_split=0.0)

    # Compute quantities required for feature-wise normalization
    # (std, mean, and principal components if ZCA whitening is applied).
    datagen.fit(x_train)

    # Fit the model on the batches generated by datagen.flow().
    model.fit(x_train, y_train,
		      batch_size=batch_size,
              epochs=epocas_totales,
              validation_data=(x_test, y_test),
              callbacks = callbacks,
              shuffle = True)


# Guardamos el modelo con los pesos
if not os.path.isdir(save_dir):
    os.makedirs(save_dir)
model_path = os.path.join(save_dir, model_name)
model.save(model_path)
print('Saved trained model at %s ' % model_path)


score = model.evaluate(x_test, y_test, batch_size=128)
plot_model(model, to_file='image.png', show_shapes=True, show_layer_names=True)
# serialize model to JSON
model_json = model.to_json()
with open("model.json", "w") as json_file:
    json_file.write(model_json)
# serialize weights to HDF5
model.save_weights("model.h5")
print("Saved model to disk")

# later...

# load json and create model
json_file = open('model.json', 'r')
loaded_model_json = json_file.read()
json_file.close()
loaded_model = model_from_json(loaded_model_json)
# load weights into new model
loaded_model.load_weights("model.h5")
print("Loaded model from disk")

# Score trained model.
scores = model.evaluate(x_test, y_test, verbose=1)
print('Test loss:', scores[0])
print('Test accuracy:', scores[1])