# https://github.com/uchidama/CIFAR10-Prediction-In-Keras/blob/master/keras_cifar10_webcam_prediction.ipynb


#Importamos los paquetes necesarios para realizar la comprobación del entrenamiento con CIFAR10
#Así como librerias utiles para el procesamiento y visualización de la imagen de entrada
from __future__ import print_function
import keras
from keras.datasets import cifar10
from keras.models import load_model
import os
import numpy as np
from keras.preprocessing import image
import matplotlib.pyplot as plt
from io import BytesIO
from PIL import Image
from PIL import ImageOps
#--------------------------------------------------------------------------------------------

#Imagen a realizar el análizis

img = image.load_img('images/caballo.jpg')


image = Image.fromarray(np.uint8(img))          #Converción de Imagen en tipo uint8
image = image.resize((32, 32))                  #Realiza el cambioo de tamaño en la imagen a 32x32
resize_img = np.asarray(image)                  #Convierte la Imagen a matriz

#--------------------------------------------------------------------------------------------
# Etiquetas que reconoce CIFAR10.
cifar10_labels = np.array([
    'avion',
    'automovil',
    'pajaro',
    'gato',
    'ciervo',
    'perro',
    'rana',
    'caballo',
    'barco',
    'camion'])



# Usamos la ruta de los pesos del modelo previamente entrenado
#----------------------------------------------------------------------------------------
#model = load_model('saved_models/keras_cifar10_trained_model.h5')
model = load_model('output/checkpoints/epoch_10.hdf5')
#----------------------------------------------------------------------------------------
#En este caso colocamos la epoca10 la cual fue la última epoca en arrojar el entrenamiento


#Función que realiza la parametrización en CIFAR10 de la imagen de entrada
def convertCIFER10Data(image):
    img = image.astype('float32')
    img /= 255
    c = np.zeros(32 * 32 * 3).reshape((1, 32, 32, 3))
    c[0] = img
    return c

#Llamada de la funcion con la imagen redimensionada
data = convertCIFER10Data(resize_img)

#Permite la visualización de la imagen
plt.imshow(img)
plt.axis('off')


# Imprime las etiquetas que predice CIFAR10
ret = model.predict(data, batch_size=1)
# print(ret)
print("")
print("----------------------------------------------")
bestnum = 0.0
bestclass = 0
for n in [0, 1, 2, 3, 4, 5, 6, 7, 8, 9]:
    print("[{}] : {}%".format(cifar10_labels[n], round(ret[0][n] * 100, 2)))
    if bestnum < ret[0][n]:
        bestnum = ret[0][n]
        bestclass = n

print("----------------------------------------------")
#Arroja los resultados de la predicción
plt.show()
print("Probabilidad : {}%".format(round(bestnum * 100, 2)))
print("Prediccion: [{}].".format(cifar10_labels[bestclass]))
