# import the necessary packages
from keras.callbacks import Callback
import os

class EpochCheckpoint(Callback):
	def __init__(self, outputPath, every=1, startAt=0):
		# call the parent constructor
		super(Callback, self).__init__()

		# almacenar la ruta de salida base para el modelo, el número de
		# épocas que deben pasar antes de que el modelo se serialice en
		# disco y el valor de época actual
		self.outputPath = outputPath
		self.every = every
		self.intEpoch = startAt

	def on_epoch_end(self, epoch, logs={}):
		# compruebe si el modelo debe serializarse en el disco
		if (self.intEpoch + 1) % self.every == 0:
			p = os.path.sep.join([self.outputPath,
				"epoch_{}.hdf5".format(self.intEpoch + 1)])
			self.model.save(p, overwrite=True)

		# incrementar el contador de época interno
		self.intEpoch += 1